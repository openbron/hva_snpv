# Copyright (c) 2013 Joey Toppin
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import socket, dns.name, dns.reversename
from dnslib import DNSRecord, DNSQuestion
import datetime
from   mod.tcp.core import *

# const.
TCP_IP   = "127.0.0.1"
TCP_PORT = 5050
BUFFER_SIZE = 1024


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
c = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

def startsrv():
    ip    = TCP_IP
    port  = TCP_PORT
    buffr = BUFFER_SIZE

    #s = tcp
    s.bind((ip, port))

    # arg. is max number of queued connections
    s.listen(1)

    print "\nSocket server running on %(ip)s:%(port)d"\
          %{"ip": ip, "port": port}

    # call a single internal client connection
    startcli()

    conn, addr = s.accept()
    print "connection address:", addr

    while 1:
        data = conn.recv(buffr)
        if not data:
          break
        print ""
        validate(data)


# stop server connection
def stopsrv():
    conn.close()


# close socket
def close():
    s.close()


# start client
def startcli():
    ip    = TCP_IP
    port  = TCP_PORT
    buffr = BUFFER_SIZE

    #c = tcp
    c.connect((ip, port))


# send a TCP message
def msg(message):
    c.send(message)


# retrieve domain ip by DNS lookup
def ip(domain):

    if(socket.gethostbyname(domain)):
        address = socket.gethostbyname(domain)
        print address

        f = open("net.log", "a")
        f.write("{0}: (DNS) ping to: {1} resolved as: {2}\n\n"
                .format(datetime.datetime.now(), domain, address))
        f.close()

    else:
        print "Could not resolve input"


# retrieve domain name by DNS reverse lookup
def name(ip):

    if(dns.reversename.from_address(ip)):
        name = dns.reversename.from_address(ip)
        resv = dns.reversename.to_address(name)
        print resv

        f = open("net.log", "a")
        f.write("{0}: (DNS) reverse lookup: {1} resolved as: {2}\n\n"
                .format(datetime.datetime.now(), ip, resv))
        f.close()

    else:
        print "Could not resolve input"


# create a DNS request packet
# parameters are stored in net.log
# any input can be given for domain
def dns(domain):

    packet = DNSRecord(q=DNSQuestion(domain))

    f = open("net.log", "a")
    f.write("{0}: DNS request package created:\n{1}\n\n"
            .format(datetime.datetime.now(), packet))
    f.close()

    print packet


# decode a DNS packet
def ddns(packet):

  u = packet.decode("hex")
  unpack = DNSRecord.parse(u)

  f = open("net.log", "a")
  f.write("{0}: Attempt to decode a DNS package:\n{1}\n\n"
            .format(datetime.datetime.now(), unpack))
  f.close()

  print unpack
